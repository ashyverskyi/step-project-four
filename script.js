

const URL = "https://ajax.test-danit.com/api/v2/cards";

const BODY = document.querySelector("body");

const someMail = "boyakkerman9@gmail.com";
const somePassword = "akkerman";


const CONFIG_GET_ALL = {
    headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${'26fc2502-6d9d-46fa-bae9-a664a7e0a41c'}`,
    },
};


function CONFIG_POST(data) {
    return {
        headers: {
            Authorization: `Bearer ${'26fc2502-6d9d-46fa-bae9-a664a7e0a41c'}`,
        },
        body: JSON.stringify(Object.fromEntries(data.entries())),
    };
}


function CONFIG_PUT(data) {
    return {
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${'26fc2502-6d9d-46fa-bae9-a664a7e0a41c'}`,
        },
        body: JSON.stringify(Object.fromEntries(data.entries())),
    };
}


const CONFIG_DEL = {
    headers: {
        Authorization: `Bearer ${'26fc2502-6d9d-46fa-bae9-a664a7e0a41c'}`,
    },
};

async function sendRequest(url, method = "GET", config = {}) {
    try {
        const response = await fetch(url, { method, ...config });

        if (!response.ok) throw new Error(`Fail to fetch from ${url}`);
        console.log(response);

        if (method === "DELETE" || method === "PUT") {
            return response;
        } else {
            return response.json();
        }
    } catch (error) {
        console.log("Error fetch data: ", error);
        throw error;
    }
}


class Cards {
   
    static switchTextEmptyCards() {
        const cardsExist = document.querySelector(".card");
        const textEmptyCards = document.querySelector("#text-empty-cards");

        if (cardsExist) {
            textEmptyCards.style.display = "none";
        } else {
            textEmptyCards.style.display = "block";
        }
    }

    
    static switchCards() {
        const cardFooter = this.createContainerCard.querySelector(".card_footer");
        const btnShowMore = cardFooter.querySelector(".btn_show_more");
        const btnShowLess = cardFooter.querySelector(".btn_show_less");
        const cardExpand = this.createContainerCard.querySelector(".card_expand");
        const cardClass = cardExpand.closest(".card");

        
        btnShowMore.addEventListener("click", () => {
            btnShowMore.style.display = "none"; 
            btnShowLess.style.display = "inline-block"; 
            cardExpand.style.display = "block"; 
            cardClass.classList.add("grid-expand"); 
        });

        
        btnShowLess.addEventListener("click", () => {
            btnShowMore.style.display = "inline-block";
            btnShowLess.style.display = "none"; 
            cardExpand.style.display = "none"; 
            cardClass.classList.remove("grid-expand"); 
        });
    }

    static getCardData(item) {
    
        this.firstName = item.firstName;
        this.lastName = item.lastName.toUpperCase();
        this.patronymic = item.patronymic;
        this.userName = item.fullName; 
        this.userId = item.id;

       
        this.specialist = item.dropdownDoctor.toUpperCase();
        this.complaint = item.title;
        this.priority = item.dropdownUrgency;
        this.status = item.status;

        
        this.age = item.age ? item.age : "";
        this.bodyHeight = item.bodyHeight ? item.bodyHeight : "";
        this.bodyWeight = item.bodyWeight ? item.bodyWeight : "";
        this.bodyMassIndex = item.bodyMassIndex ? item.bodyMassIndex : "";
        this.dateLastVisit = item.dateLastVisit ? item.dateLastVisit : "";
        this.description = item.description;
        this.normalPressure = item.normalPressure ? item.normalPressure : "";
        this.transferredCardiacDiseases = item.transferredCardiacDiseases
            ? item.transferredCardiacDiseases
            : "";
    }

    static createCardElement() {
        const boardCards = document.querySelector("#board_cards"); 
        this.createContainerCard = document.createElement("div"); 
        this.createContainerCard.classList.add("card", "border", "shadow"); 
        this.createContainerCard.dataset.idCard = this.userId; 

        
        if (this.specialist.toLowerCase() === "cardiologist") {
            this.createContainerCard.insertAdjacentHTML(
                "beforeend",
                `
      <button class="btn__del btn_delete_card"></button>
      <button class="btn__edit btn_edit_card"></button>
      <div class="card__wrap">
        <div class="card__header">
          <p class="card__header-number">№ ${this.userId}</p>
          <p class="card_header-name-last-name card_lastName">${this.lastName}</p>
          <p class="card__header-username card_firstName">${this.firstName} ${this.patronymic}</p>
        </div>

        <div class="card__body">
          <div class="card__base">
              <p class="card__patient card_specialist"><span class="bold">To a Specialist: </span><span class="card_specialist_id">${this.specialist}</span></p>
              <p class="card__patient card_complaint"><span class="bold">Complaint: </span> ${this.complaint}</p>
              <p class="card__patient card_priority">
                <span class="bold">Priority:</span> ${this.priority}
              </p>
              <p class="card__patient card_status"><span class="bold">Status:</span> ${this.status}</p>
          </div>
          
          <div class="card__expand card_expand" style="display: none">
              <p class="card__patient card_age"><span class="bold">Age:</span> ${this.age}</p>
              <p class="card__patient card_bodyHeight">
                  <span class="bold">Body Height:</span> ${this.bodyHeight}</p>
              <p class="card__patient card_bodyWeight"><span class="bold">Body Weight:</span> ${this.bodyWeight}</p>
              <p class="card__patient card_bodyMassIndex">
                  <span class="bold">Body Mass Index:</span> ${this.bodyMassIndex}
              </p>              
              <p class="card__patient card_description"><span class="bold">Description:</span> ${this.description}</p>
              <p class="card__patient card_normalPressure">
                  <span class="bold">Normal Pressure:</span> ${this.normalPressure}
              </p>
              <p class="card__patient card_transferredCardiacDiseases"><span class="bold">Transferred Cardiac Diseases:</span> ${this.transferredCardiacDiseases}</p>
          </div>
        </div>
        <div class="card__footer card_footer">
          <button class="btn card__btn btn_show_more">Show more</button>
          <button class="btn card__btn btn_show_less" style="display: none">Show less</button>
        </div>
      </div>
          `
            );
        } else if (this.specialist.toLowerCase() === "dentist") {
            this.createContainerCard.insertAdjacentHTML(
                "beforeend",
                `
      <button class="btn__del btn_delete_card"></button>
      <button class="btn__edit btn_edit_card"></button>
      <div class="card__wrap">
        <div class="card__header">
          <p class="card__header-number">№ ${this.userId}</p>
          <p class="card_header-name-last-name card_lastName">${this.lastName}</p>
          <p class="card__header-username card_firstName">${this.firstName} ${this.patronymic}</p>
        </div>

        <div class="card__body">
          <div class="card__base">
              <p class="card__patient card_specialist"><span class="bold">To a Specialist: </span><span class="card_specialist_id">${this.specialist}</span></p>
              <p class="card__patient card_complaint"><span class="bold">Complaint: </span> ${this.complaint}</p>
              <p class="card__patient card_priority">
                <span class="bold">Priority:</span> ${this.priority}
              </p>
              <p class="card__patient card_status"><span class="bold">Status:</span> ${this.status}</p>
          </div>
          
          <div class="card__expand card_expand" style="display: none">
              <p class="card__patient card_description"><span class="bold">Description:</span> ${this.description}</p>
              <p class="card__patient card_dateLastVisit">
                  <span class="bold">The Date of Last Visit:</span> ${this.dateLastVisit}
              </p>
          </div>
        </div>
        <div class="card__footer card_footer">
          <button class="btn card__btn btn_show_more">Show more</button>
          <button class="btn card__btn btn_show_less" style="display: none">Show less</button>
        </div>
      </div>
          `
            );
        } else {
            this.createContainerCard.insertAdjacentHTML(
                "beforeend",
                `
      <button class="btn__del btn_delete_card"></button>
      <button class="btn__edit btn_edit_card"></button>
      <div class="card__wrap">
        <div class="card__header">
          <p class="card__header-number">№ ${this.userId}</p>
          <p class="card_header-name-last-name card_lastName">${this.lastName}</p>
          <p class="card__header-username card_firstName">${this.firstName} ${this.patronymic}</p>
        </div>

        <div class="card__body">
          <div class="card__base">
              <p class="card__patient card_specialist"><span class="bold">To a Specialist: </span><span class="card_specialist_id">${this.specialist}</span></p>
              <p class="card__patient card_complaint"><span class="bold">Complaint: </span> ${this.complaint}</p>
              <p class="card__patient card_priority">
                <span class="bold">Priority:</span> ${this.priority}
              </p>
              <p class="card__patient card_status"><span class="bold">Status:</span> ${this.status}</p>
          </div>
          
          <div class="card__expand card_expand" style="display: none">
              <p class="card__patient card_age"><span class="bold">Age:</span> ${this.age}</p>
              <p class="card__patient card_description"><span class="bold">Description:</span> ${this.description}</p>
          </div>
        </div>
        <div class="card__footer card_footer">
          <button class="btn card__btn btn_show_more">Show more</button>
          <button class="btn card__btn btn_show_less" style="display: none">Show less</button>
        </div>
      </div>
          `
            );
        }

        boardCards.append(this.createContainerCard);

        Cards.switchCards();

        Cards.switchTextEmptyCards();
    }

    static updateCardElement(cardId, newData) {
        const cardToUpdate = document.querySelector(`[data-id-card="${cardId}"]`);
        if (!cardToUpdate) {
            console.error("Card not found!");
            return;
        }

        cardToUpdate.querySelector(".card_lastName").textContent = newData.lastName;
        cardToUpdate.querySelector(".card_firstName").textContent =
            newData.firstName + " " + newData.patronymic;

        cardToUpdate.querySelector(
            ".card_specialist"
        ).innerHTML = `<span class="bold">Specialist: </span> ${newData.dropdownDoctor}`;
        cardToUpdate.querySelector(
            ".card_complaint"
        ).innerHTML = `<span class="bold">Complaint: </span> ${newData.title}`;
        cardToUpdate.querySelector(
            ".card_priority"
        ).innerHTML = `<span class="bold">Priority:</span> ${newData.dropdownUrgency}`;
        cardToUpdate.querySelector(
            ".card_status"
        ).innerHTML = `<span class="bold">Status:</span> ${newData.status}`;

        cardToUpdate.querySelector(
            ".card_status"
        ).innerHTML = `<span class="bold">Status:</span> ${newData.status}`;
    }
}

const cards = new Cards();


class VisitFormBase {
    constructor() {
        this.formaVisitCreate = document.getElementById("modal-visit__create"); 
        this.choiceDoctor = document.getElementById("modal-visit__dropdownDoctor"); 
        this.commonFieldsContainer = document.getElementById("common_fields_container"); 
        this.fieldsContainers = document.querySelectorAll(".fields_container");
        this.lastNameInput = document.getElementById("modal-visit__lastName"); 
        this.firstNameInput = document.getElementById("modal-visit__firstName"); 
        this.patronymicInput = document.getElementById("modal-visit__patronymic"); 
        this.fullNameInput = document.getElementById("modal-visit__fullName"); 
        this.selectedDoctor = null; 
        this.setupEventListener();
    }

    
    setupEventListener() {
        this.choiceDoctor.addEventListener("change", this.onDoctorChange.bind(this));
        this.lastNameInput.addEventListener("input", this.updateFullName.bind(this));  
        this.firstNameInput.addEventListener("input", this.updateFullName.bind(this));
        this.patronymicInput.addEventListener("input", this.updateFullName.bind(this));
    }

  
    onDoctorChange() {
        
    }

    
    showEditForm() {
        this.hideAllFieldsContainers();
        this.showCommonFields(); 
        this.updateDefaultFields();
    }

  
    hideAllFieldsContainers() {
        this.fieldsContainers.forEach(container => {
            container.setAttribute("data-show", "hidden");
        });
    }

   
    showCommonFields() {
        this.commonFieldsContainer.removeAttribute("data-show");
    }

   
    updateFullName() {
        const fullName = `${this.lastNameInput} ${this.firstNameInput} ${this.patronymicInput}`;
        this.fullNameInput.value = fullName;
    }  

    
    disableNonRelevantFields() {
        const selectedDoctorType = this.choiceDoctor.value;
        this.fieldsContainers.forEach(container => {
            const containerId = container.id;
            if (containerId !== selectedDoctorType && containerId !== 'common_fields_container') {
                container.querySelectorAll("input, select, textarea").forEach(item => {
                    item.disabled = true;
                });
            }
        });
    }

    
    removeAllDisabled() {
        this.fieldsContainers.forEach(container => {
            container.querySelectorAll("input, select, textarea").forEach(item => {
                item.disabled = false;
            });
        });
    }

    updateDefaultFields() {
        document.getElementById("modal-visit__firstName");
        document.getElementById("modal-visit__lastName");
        document.getElementById("modal-visit__patronymic")
        document.getElementById("modal-visit__fullName");
        document.getElementById("modal-visit__title");
        document.getElementById("modal-visit__description");
        document.getElementById("modal-visit__dropdownUrgency_dentist");
    }
}



class VisitDentist extends VisitFormBase {
    constructor() {
        super();
        this.dentistFieldsContainer = document.getElementById("dentist");
    }

   
    onDoctorChange() {
        if (this.choiceDoctor.value === "dentist") {
            this.hideAllFieldsContainers();
            this.showCommonFields(); 
            this.dentistFieldsContainer.removeAttribute("data-show");
            this.updateDefaultFields();
            this.removeAllDisabled();  
            this.disableNonRelevantFields(); 
        }
    }

    
    showEditForm() {
        this.hideAllFieldsContainers(); 
        this.showCommonFields();
        this.dentistFieldsContainer.removeAttribute("data-show"); 
        this.updateDefaultFields();
        this.removeAllDisabled(); 
        this.disableNonRelevantFields();  
    }

  
    updateDefaultFields() {
        document.getElementById("modal-visit__firstName");
        document.getElementById("modal-visit__lastName");
        document.getElementById("modal-visit__patronymic");
        document.getElementById("modal-visit__fullName");
        document.getElementById("modal-visit__title").value = "";
        document.getElementById("modal-visit__description").value = "";
        document.getElementById("modal-visit__dropdownUrgency_dentist").value = "Low";
        document.getElementById("modal-visit__dateLastVisit").value = "2024-01-01";
    }
}

new VisitDentist();


class VisitCardiologist extends VisitFormBase {
    constructor() {
        super();
        this.cardiologistFieldsContainer = document.getElementById("cardiologist");
        this.weightInput = document.getElementById("bodyWeight"); 
        this.heightInput = document.getElementById("bodyHeight"); 
        this.bmiInput = document.getElementById("modal-visit__bodyMassIndex"); 
        this.weightInput.addEventListener("input", this.calculateBMI.bind(this));
        this.heightInput.addEventListener("input", this.calculateBMI.bind(this));
    }

    
    onDoctorChange() {
        if (this.choiceDoctor.value === "cardiologist") {
            this.hideAllFieldsContainers(); 
            this.showCommonFields(); 
            this.cardiologistFieldsContainer.removeAttribute("data-show");
            this.updateDefaultFields();
            this.removeAllDisabled();  
            this.disableNonRelevantFields();  
        }
    }

    
    showEditForm() {
        this.hideAllFieldsContainers(); 
        this.showCommonFields();
        this.cardiologistFieldsContainer.removeAttribute("data-show");
        this.updateDefaultFields();
        this.removeAllDisabled(); 
        this.disableNonRelevantFields();  
    }

    
    calculateBMI() {
        const weight = parseFloat(this.weightInput.value); 
        const height = parseFloat(this.heightInput.value);
        if (weight > 0 && height > 0) {
            const heightInMeters = height / 100;  
            const bmi = weight / (heightInMeters * heightInMeters);
            this.bmiInput.value = bmi.toFixed(2);
        } else {
            this.bmiInput.value = "";
        }
    }

   
    updateDefaultFields() {
        document.getElementById("modal-visit__firstName");
        document.getElementById("modal-visit__lastName");
        document.getElementById("modal-visit__patronymic");
        document.getElementById("modal-visit__fullName");
        document.getElementById("modal-visit__title").value = "";
        document.getElementById("modal-visit__description").value = "";
        document.getElementById("modal-visit__dropdownUrgency").value = "Low";
        document.getElementById("modal-visit__normalPressure").value = "120/80";
        document.getElementById("modal-visit__age").value = "";
        document.getElementById("bodyWeight").value = "";
        document.getElementById("bodyHeight").value = "";
        document.getElementById("modal-visit__bodyMassIndex").value = "";
        document.getElementById("modal-visit__transferredCardiacDiseases").value = "";
    }
}

new VisitCardiologist();


class VisitTherapist extends VisitFormBase {
    constructor() {
        super();
        this.therapistFieldsContainer = document.getElementById("therapist");
    }

    
    onDoctorChange() {
        if (this.choiceDoctor.value === "therapist") {
            this.hideAllFieldsContainers(); 
            this.showCommonFields(); 
            this.therapistFieldsContainer.removeAttribute("data-show");
            this.updateDefaultFields();
            this.removeAllDisabled();  
            this.disableNonRelevantFields();   
        }
    }

    
    showEditForm() {
        this.hideAllFieldsContainers(); 
        this.showCommonFields(); 
        this.therapistFieldsContainer.removeAttribute("data-show");
        this.updateDefaultFields();
        this.removeAllDisabled();  
        this.disableNonRelevantFields();  
    }

    
    updateDefaultFields() {
        document.getElementById("modal-visit__firstName");
        document.getElementById("modal-visit__lastName");
        document.getElementById("modal-visit__patronymic");
        document.getElementById("modal-visit__fullName");
        document.getElementById("modal-visit__title");
        document.getElementById("modal-visit__description");
        document.getElementById("modal-visit__dropdownUrgency-therapist");
        document.getElementById("modal-visit__age-therapist");
        document.getElementById("modal-visit__status-therapist");
    }
}

new VisitTherapist();


class ModalVisitFormRender {
    constructor() {
        this.choiceDoctor = document.getElementById("modal-visit__dropdownDoctor"); 
        this.fieldsContainers = document.querySelectorAll(".fields_container"); 
        this.createVisitFormWrapper = document.getElementById("modal-visit__form-wrapper");
        this.btnCreateVisit = document.getElementById("btn_create_visit");

        this.btnCloseCreateVisit = document.getElementById("modal-visit__btn-close");
        this.visitForm = document.getElementById("modal-visit__create"); 

        this.doctorType = null; 
        this.visitFormBase = new VisitFormBase(); 
        this.visitDentist = new VisitDentist(); 
        this.visitCardiologist = new VisitCardiologist(); 
        this.visitTherapist = new VisitTherapist(); 

        this.setupEventListener();
    }

    
    openModalCreateVisitForm() {
        this.createVisitFormWrapper.style.display = "block";
        this.resetForm(); 
    }

    
    openModalEditVisitFormWithSelectedDoctor(specialistValue) {
        this.createVisitFormWrapper.style.display = "block";
        this.choiceDoctor.value = specialistValue;  

        if (specialistValue === "dentist") {
            this.visitDentist.showEditForm(); 
        } else if (specialistValue === "cardiologist") {
            this.visitCardiologist.showEditForm(); 
        } else if (specialistValue === "therapist") {
            this.visitTherapist.showEditForm(); 
        } else {
            this.visitFormBase.showEditForm(); 
        }
    }

    
    closeModalCreateVisitForm() {
        this.createVisitFormWrapper.style.display = "none";
    }

    
    resetForm() {
        this.visitForm.reset(); 
        this.fieldsContainers.forEach(container => {
            container.setAttribute("data-show", "hidden"); 
        });
        this.choiceDoctor.selectedIndex = 0; 
    }

    
    setupEventListener() {
        this.btnCreateVisit.addEventListener("click", () => this.openModalCreateVisitForm());
        this.btnCloseCreateVisit.addEventListener("click", () => this.closeModalCreateVisitForm());
        this.visitForm.addEventListener("submit", (event) => {
            event.preventDefault();
            this.closeModalCreateVisitForm();
        });
        
        this.createVisitFormWrapper.addEventListener("mousedown", (event) => {
            if (!this.visitForm.contains(event.target)) {
                this.closeModalCreateVisitForm();
            }
        });
       
       
        
        const submitButton = document.getElementById("btn_submit_form");
        submitButton.addEventListener("keydown", (event) => {
            if (event.key === "Enter") {
                event.preventDefault(); 
                submitButton.click(); 
            }
        });
    }
}

new ModalVisitFormRender();


class DataService {
    constructor() {
        this.renderForm = new ModalVisitFormRender(); 

        this.form = document.querySelector("#modal-visit__create"); 
        this.cardsBoard = document.querySelector("#board_cards"); 
        this.btnCreateCard = document.querySelector("#btn_submit_form"); 
        this.btnEditForm = document.querySelector("#btn_edit_form");  

        this.init();
        this.createCard();
        this.callEditForm();
        this.deleteCard();
    }

    
    async init() {
        await this.getDataBase();
        await this.addEditFormListener();
    }

    async getDataBase() {
        try {
            const allDataBase = await sendRequest(URL, "GET", CONFIG_GET_ALL);
            console.log("Exist database: ", allDataBase);

            allDataBase.forEach((item) => {
                Cards.getCardData(item);

                Cards.createCardElement(); 
            });
        } catch (error) {
            console.error("Some mistake:", error);
        }
    }

    
    async createCard() {
        this.form.addEventListener("submit", (event) => this.formSubmit(event)); 
    }

    
    async formSubmit(event) {
        event.preventDefault();
        const formData = new FormData(this.form); 

        try {
            const response = await sendRequest(URL, "POST", CONFIG_POST(formData));
            console.log(response);
            Cards.getCardData(response);
            Cards.createCardElement(); 
        } catch (error) {
            console.error("Some mistake:", error);
        }
    }

    
    callEditForm() {
        this.cardsBoard.addEventListener("click", async (event) => {
            event.preventDefault();

            this.btnEditCard = event.target.closest(".btn_edit_card"); 
            if (!this.btnEditCard) return;

            this.findEditCard = this.btnEditCard.closest(".card"); 
            this.getIdCard = this.findEditCard.dataset.idCard; 

            this.findSpecialist = this.findEditCard.querySelector(
                ".card_specialist_id"
            );

            const specialistValue = this.findSpecialist.textContent.toLowerCase();
            console.log(specialistValue);

            this.renderForm.openModalEditVisitFormWithSelectedDoctor(specialistValue);

            try {
                const cardData = await this.getExistingCardDataFromServer(this.getIdCard);
                this.populateFormFields(cardData); 
            } catch (error) {
                console.error("Error calling edit form:", error);
            }

            this.swichBtnEditVsCreate();
        });
    }

    async getExistingCardDataFromServer(getIdCard) {
        try {
            const cardData = await sendRequest(`${URL}/${getIdCard}`, "GET", CONFIG_GET_ALL);
            console.log("Отримали об'єкт картки для подальшого редагування: ", cardData);
            return cardData;
        } catch (error) {
            console.error("Error fetching card data:", error);
            throw error;
        }
    }

    populateFormFields(cardData) {
        document.getElementById("modal-visit__lastName").value = cardData.lastName;
        document.getElementById("modal-visit__firstName").value = cardData.firstName;
        document.getElementById("modal-visit__patronymic").value = cardData.patronymic;
        document.getElementById("modal-visit__fullName").value = cardData.fullName;
        document.getElementById("modal-visit__title").value = cardData.title;
        document.getElementById("modal-visit__description").value = cardData.description;
        document.getElementById("modal-visit__dateLastVisit").value = cardData.dateLastVisit;
        document.getElementById("modal-visit__dropdownUrgency").value = cardData.dropdownUrgency;
        document.getElementById("modal-visit__dropdownUrgency_dentist").value = cardData.dropdownUrgency;
        document.getElementById("modal-visit__dropdownUrgency-therapist").value = cardData.dropdownUrgency;
        document.getElementById("modal-visit__status").value = cardData.status;
        document.getElementById("modal-visit__status-dentist").value = cardData.status;
        document.getElementById("modal-visit__status-therapist").value = cardData.status;
        document.getElementById("modal-visit__normalPressure").value = cardData.normalPressure;
        document.getElementById("modal-visit__age").value = cardData.age;
        document.getElementById("modal-visit__age-therapist").value = cardData.age;
        document.getElementById("bodyWeight").value = cardData.bodyWeight;
        document.getElementById("bodyHeight").value = cardData.bodyHeight;
        document.getElementById("modal-visit__bodyMassIndex").value = cardData.bodyMassIndex;
        document.getElementById("modal-visit__transferredCardiacDiseases").value = cardData.transferredCardiacDiseases;
    }

    async addEditFormListener() {
        
        this.btnEditForm.addEventListener("click", async (event) => {
            event.preventDefault();
            this.form = document.querySelector("#modal-visit__create"); 
            this.formDataEdit = new FormData(this.form); 

            
            try {
                const response = await sendRequest(
                    `${URL}/${this.getIdCard}`,
                    "PUT",
                    CONFIG_PUT(this.formDataEdit)
                );
                const jsonResponse = await response.json();
                console.log(jsonResponse);
                Cards.updateCardElement(this.getIdCard, jsonResponse);
            } catch (error) {
                console.error("Mistake with the PUT method:", error);
            }

            this.closeEditForm(); 
        });
    }

    
    swichBtnEditVsCreate() {
        this.btnCreateCard.style.display = "none";
        this.btnEditForm.style.display = "block";
    }

    
    closeEditForm() {
        this.btnCreateCard.style.display = "block";
        this.btnEditForm.style.display = "none";
        this.renderForm.closeModalCreateVisitForm();
    }

    
    async deleteCard() {
        try {
            this.cardsBoard.addEventListener("click", (event) => {
                const btnDelCard = event.target.closest(".btn_delete_card");
                if (!btnDelCard) return;

                const getCard = btnDelCard.closest(".card"); 
                const getIdCard = getCard.dataset.idCard;

                sendRequest(`${URL}/${getIdCard}`, "DELETE", CONFIG_DEL);

                getCard.remove(); 

                Cards.switchTextEmptyCards(); 
            });
        } catch (error) {
            console.error("Some mistake:", error);
        }
    }
}




function startCards() {
    new DataService();
}

let responseToken;
let enterswich = 0;
const btnSignIn = document.querySelector("#btn_sign_in");
const btnVisit = document.querySelector("#btn_create_visit");
const btnExit = document.querySelector("#btn_exit");




class Entrance {
    constructor() {
        this.getInnerStyles = document.querySelectorAll(".inner"); 
        this.getSearchElements = document.querySelectorAll(".search_style"); 
        this.initializeFilters();
    }

    initializeFilters() {
        const inputText = document.querySelector('.nav__search-arr');
        const statusSelectItems = document.querySelectorAll('.status_current, .select__body .select__item');
        const prioritySelectItems = document.querySelectorAll('.priority_current, .select__body .select__item');
        const listCards = document.querySelectorAll('.card');


        inputText.oninput = () => {
            this.filterCards(listCards, inputText.value);
        };


        statusSelectItems.forEach(item => {
            item.addEventListener('click', () => {
                document.querySelector('.status_current').textContent = item.textContent;
                this.filterCards(listCards, inputText.value);
            });
        });


        prioritySelectItems.forEach(item => {
            item.addEventListener('click', () => {
                document.querySelector('.priority_current').textContent = item.textContent;
                this.filterCards(listCards, inputText.value);
            });
        });
    }

    filterCards(listCards, searchValue) {
        let selectedStatus = document.querySelector('.status_current').textContent;
        let selectedPriority = document.querySelector('.priority_current').textContent;
        searchValue = searchValue.trim().toLowerCase();

        listCards.forEach((card) => {
            let cardText = card.innerText.toLowerCase();
            let cardStatus = card.getAttribute('data-status'); 
            let cardPriority = card.getAttribute('data-priority'); 

            let matchesSearch = cardText.includes(searchValue);
            let matchesStatus = (selectedStatus === 'All' || selectedStatus === cardStatus);
            let matchesPriority = (selectedPriority === 'All' || selectedPriority === cardPriority);

            if (matchesSearch && matchesStatus && matchesPriority) {
                card.classList.remove('hidden');
            } else {
                card.classList.add('hidden');
            }
        });
    }

    createModalEntrance() {
        btnSignIn.addEventListener("click", () => {
            const findEntranseModule = document.querySelector(".entrance"); 
            if (findEntranseModule === null) {
                const createGeneralEntry = document.createElement("div");
                createGeneralEntry.classList.add("entrance", "border", "shadow");

                createGeneralEntry.insertAdjacentHTML(
                    "beforeend",
                    `
                    <div>
                      <button class="btn__close modal_entrence_close">X</button>
                      <form class="entrance__form user_data">
                      <h1>Email:</h1>
                        <input
                          type="email"
                          class="entrance__logo user_email"
                          placeholder="${someMail}"
                          title="email"
                          autofocus="logo"
                          name="logo"
                        />
                        <h1>Password:</h1>
                        <input
                          type="password"
                          class="btn-general btn-field user_pass"
                          placeholder="${somePassword}"
                          title="password"
                          name="password"
                        />
                        <input type="reset" value="Sign In" class="btn usersign_btn" />
                      </form>
                    </div>
                    `
                );
                BODY.append(createGeneralEntry);
            }
            this.checkUserData();
            this.deleteModalSign(); 
            this.createCloakMode(); 
        });

        btnExit.addEventListener("click", () => {
            btnExit.classList.toggle("hidden");
            btnSignIn.classList.toggle("hidden");
            const currentCards = document.querySelectorAll(".card");
            currentCards.forEach((element) => {
                element.remove();
                responseToken = "";
                enterswich = 0;
                btnVisit.classList.add("hidden");
            });
        });
    }

    deleteModalSign() {
        const btnClose = document.querySelector(".modal_entrence_close");
        const modalEntrance = document.querySelector(".entrance");

        BODY.addEventListener("keyup", (event) => {
            if (event.code === "Escape") {
                modalEntrance.remove();
                this.removeCloakMode(); 
            }
        });

        btnClose.addEventListener("click", () => {
            modalEntrance.remove();
            this.removeCloakMode(); 
        });

        if (responseToken === '26fc2502-6d9d-46fa-bae9-a664a7e0a41c' && !enterswich) {
            modalEntrance.remove();
            startCards();
            btnSignIn.classList.toggle("hidden");
            btnExit.classList.toggle("hidden");
            btnVisit.classList.remove("hidden");
            enterswich = 1;
            this.removeCloakMode(); 
        }
    }

    checkUserData() {
        const userSignBtn = document.querySelector(".usersign_btn");
        userSignBtn.addEventListener("click", () => {
            if (responseToken !== '26fc2502-6d9d-46fa-bae9-a664a7e0a41c') {
                this.getToken(someMail, somePassword);
            }
        });
    }

    getToken(email, password) {
        fetch(`${URL}/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                email: "boyakkerman9@gmail.com",
                password: "akkerman",
            }),
        })
        .then((response) => response.text())
        .then((token) => {
            responseToken = token;
            if (responseToken === '26fc2502-6d9d-46fa-bae9-a664a7e0a41c') {
                this.deleteModalSign();
            }
        });
    }

    createCloakMode() {
        this.getInnerStyles.forEach((element) => {
            element.style.backgroundColor = `#ced7df`;
            element.style.opacity = "0.50";
        });

        this.getSearchElements.forEach((element) => {
            element.style.backgroundColor = `#ced7df`;
            element.style.opacity = "0.50";
        });
    }

    removeCloakMode() {
        this.getInnerStyles.forEach((element) => {
            element.style.backgroundColor = "";
            element.style.opacity = "1";
        });

        this.getSearchElements.forEach((element) => {
            element.style.backgroundColor = "";
            element.style.opacity = "1";
        });
    }
}

const entrance = new Entrance();
entrance.createModalEntrance();

let inputText = document.querySelector('.nav__search-arr');
let searchArr = document.querySelector('.search');

inputText.oninput = function () {
    let value = this.value.trim().toLowerCase();
    let listCards = document.querySelectorAll('.card');
    listCards.forEach((card) => {
        if (card.innerText.toLowerCase().search(value) == -1) {
            card.classList.add('hidden');
        } else {
            card.classList.remove('hidden');
        }
    });
};

searchArr?.addEventListener('click', (event) => {
    event.preventDefault();
    let searchStatus = searchArr.querySelector('.status_current');
    let priorityCurrent = searchArr.querySelector('.priority_current');
    let listCards = document.querySelectorAll('.card');

    listCards.forEach((card) => {
        let status = card.getAttribute('data-status');
        let priority = card.getAttribute('data-priority');
        let matchesStatus = (!status || !searchStatus || searchStatus.textContent.toLowerCase() === 'all' || searchStatus.textContent.toLowerCase() === status.toLowerCase());
        let matchesPriority = (!priority || !priorityCurrent || priorityCurrent.textContent.toLowerCase() === 'all' || priorityCurrent.textContent.toLowerCase() === priority.toLowerCase());
        
        if (matchesStatus && matchesPriority) {
            card.classList.remove('hidden');
        } else {
            card.classList.add('hidden');
        }
    });
});

let select = function () {
    const selectHeader = document.querySelectorAll('.select__header');
    const selectItem = document.querySelectorAll('.select__item');
    const selectBlocks = document.querySelectorAll('.select');

    BODY.addEventListener('keyup', (event) => {
        selectBlocks.forEach((item) => {
            if (event.code === 'Escape' && item.classList.contains('is-active')) {
                item.classList.remove('is-active');
            }
        });
    });


    BODY.addEventListener('click', (event) => {
        selectBlocks.forEach((element) => {
            if (!event.target.closest('.select')) {
                element.classList.remove('is-active');
            }
        });
    });

    selectHeader.forEach((item) => {
        item.addEventListener('click', selectToggle);
    });

    selectItem.forEach(item => {
        item.addEventListener('click', selectChoose);
    });

    function selectToggle() {
        this.parentElement.classList.toggle('is-active');
    }

    function selectChoose() {
        let text = this.innerText,
            select = this.closest('.select'),
            currentText = select.querySelector('.select__current');
        currentText.innerText = text;
        select.classList.remove('is-active');
    }
}
select();



